// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import {
  getDatabase,
  onValue,
  ref,
  set,
  child,
  get,
  update,
  remove,
} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

import {
  getStorage,
  ref as refS,
  uploadBytes,
  getDownloadURL,
} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDvogGStUvKecoe2r2AwtUQ5RtLFihzUCg",
  authDomain: "webdatabase-319ce.firebaseapp.com",
  projectId: "webdatabase-319ce",
  storageBucket: "webdatabase-319ce.appspot.com",
  messagingSenderId: "737744255296",
  appId: "1:737744255296:web:47e1dc5f4142db234f62a5",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

const btnAgregar = document.getElementById("btnAgregar");
const btnConsultar = document.getElementById("btnConsultar");
const btnActualizar = document.getElementById("btnActualizar");
const btnBorrar = document.getElementById("btnBorrar");
const btnTodos = document.getElementById("btnTodos");
const btnLimpiar = document.getElementById("btnLimpiar");
const btnVerImagen = document.getElementById("verImagen");
const archivos = document.getElementById("archivo");

//Insertar variables inputs
var matricula = 0;
var nombre = "";
var carrera = "";
var genero = "";
var url = "";
var archivo = "";

function leerInputs() {
  matricula = document.getElementById("matricula").value;
  nombre = document.getElementById("nombre").value;
  carrera = document.getElementById("carrera").value;
  genero = document.getElementById("genero").value;
  archivo = document.getElementById("archivo").value;
  url = document.getElementById("url").value;

  //alert("  Matricula  " + matricula + "  Nombre  " + nombre + "  Carrera  " + carrera + "  Genero  " + genero);
}

function insertarDatos() {
  leerInputs();
  set(ref(db, "alumnos/" + matricula), {
    nombre: nombre,
    carrera: carrera,
    genero: genero,
  })
    .then((res) => {
      alert("Se Inserto con exito");
    })
    .catch((error) => {
      alert("Surgio un error " + error);
    });
}

function mostrarDatos() {
  leerInputs();
  const dbref = ref(db);
  get(child(dbref, "alumnos/" + matricula))
    .then((snapshot) => {
      if (snapshot.exists()) {
        nombre = snapshot.val().nombre;
        carrera = snapshot.val().carrera;
        genero = snapshot.val().genero;
        escribirInputs();
      } else {
        alert("No se encontro el registro ");
      }
    })
    .catch((error) => {
      alert("Surgio un error " + error);
    });
}

function actualizar() {
  leerInputs();
  update(ref(db, "alumnos/" + matricula), {
    nombre: nombre,
    carrera: carrera,
    genero: genero,
  })
    .then(() => {
      alert("Se realizo actualizacion");
      mostrarAlumnos();
    })
    .catch(() => {
      alert("Causo Error " + error);
    });
}

function borrar() {
  leerInputs();
  remove(ref(db, "alumnos/" + matricula))
    .then(() => {
      alert("Se borro con exito");
      mostrarAlumnos();
    })
    .catch(() => {
      alert("Causo Error " + error);
    });
}

function mostrarAlumnos() {
  const db = getDatabase();
  const dbRef = ref(db, "alumnos");

  onValue(
    dbRef,
    (snapshot) => {
      lista.innerHTML = "";
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();

        lista.innerHTML =
          "<div class='campo'>" +
          lista.innerHTML +
          childKey +
          " | " +
          childData.nombre +
          " | " +
          childData.carrera +
          " | " +
          childData.genero +
          "<br></div>";
        console.log(childKey + ":");
        console.log(childData.nombre);
      });
    },
    {
      onlyOnce: true,
    }
  );
}

function limpiar() {
  lista.innerHTML = "";
  matricula = "";
  nombre = "";
  carrera = "";
  genero = 1;
  escribirInputs();
}

function escribirInputs() {
  document.getElementById("matricula").value = matricula;
  document.getElementById("nombre").value = nombre;
  document.getElementById("carrera").value = carrera;
  document.getElementById("genero").value = genero;
}

function cargarImagen() {
  const file = event.target.files[0];
  const name = event.target.files[0].name;
  document.getElementById("imgNombre").value = name;

  const storage = getStorage();
  const storageRef = refS(storage, "imagenes/" + name);
  uploadBytes(storageRef, file).then((snapshot) => {
    alert("Se cargo la imagen");
  });
}

//btnAgregar.addEventListener('click', leerInputs);
btnAgregar.addEventListener("click", insertarDatos);
btnConsultar.addEventListener("click", mostrarDatos);
btnActualizar.addEventListener("click", actualizar);
btnBorrar.addEventListener("click", borrar);
btnTodos.addEventListener("click", mostrarAlumnos);
btnLimpiar.addEventListener("click", limpiar);
archivos.addEventListener("change", cargarImagen);
btnVerImagen.addEventListener("click", descargarImagen);
